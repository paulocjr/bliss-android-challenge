package com.bliss.challenge.viewmodel.base

import android.view.View
import androidx.databinding.ObservableField
import androidx.lifecycle.ViewModel

/**
 * Created by paulo on 15-10-2019.
 */
open class BaseViewModel : ViewModel() {

    val showLoading = ObservableField<Int>()

    init {
        showLoading.set(View.GONE)
    }

    fun showLoading() = showLoading.set(View.VISIBLE)
    fun hideLoading() = showLoading.set(View.GONE)

}