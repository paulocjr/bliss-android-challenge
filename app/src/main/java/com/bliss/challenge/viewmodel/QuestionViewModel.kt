package com.bliss.challenge.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.bliss.challenge.models.BaseData
import com.bliss.challenge.models.QuestionModel
import com.bliss.challenge.models.toModel
import com.bliss.challenge.repository.question.QuestionRepository
import com.bliss.challenge.service.models.HealthStatus
import com.bliss.challenge.service.models.Question
import com.bliss.challenge.viewmodel.base.BaseViewModel
import java.util.ArrayList

/**
 * Created by paulo on 15-10-2019.
 */
class QuestionViewModel(questionRepository: QuestionRepository) : BaseViewModel() {

    private var repository: QuestionRepository = questionRepository
    var questionsLimit = 10
    var questionsOffset = 0
    var filter : String = ""
    private var questionModelList: MutableList<QuestionModel> = ArrayList()
    private val mData = MutableLiveData<BaseData<List<QuestionModel>>>()

    /**
     * Retrieve all questions using filters
     */
    fun getQuestions(): LiveData<BaseData<List<QuestionModel>>> {

        return Transformations.map(repository.getQuestions(questionsLimit, questionsOffset, filter)) { data ->
            data.let {

                if (it.success) {
                    questionModelList.addAll(transformQuestionList(it.data!!))
                    mData.value = BaseData(true, questionModelList)
                } else {
                    mData.value = BaseData(false, null)
                }

                hideLoading()

                mData.value
            }
        }
    }

    /**
     * Retrieve the question by identifier
     */
    fun getQuestionById(id: Int): LiveData<BaseData<QuestionModel>> {
        val mData = MutableLiveData<BaseData<QuestionModel>>()

        return Transformations.map(repository.getQuestionById(id)) { data ->
            data.let {

                if (it.success) {
                    mData.value = BaseData(true, it.data?.toModel())
                } else {
                    mData.value = BaseData(false, null)
                }

                hideLoading()

                mData.value
            }
        }
    }

    /**
     * Update the question by identifier
     */
    fun updateQuestion(id: Int): LiveData<BaseData<QuestionModel>> {
        val mData = MutableLiveData<BaseData<QuestionModel>>()

        return Transformations.map(repository.updateQuestion(id)) { data ->
            data.let {

                if (it.success) {
                    mData.value = BaseData(true, it.data?.toModel())
                } else {
                    mData.value = BaseData(false, null)
                }

                hideLoading()

                mData.value
            }
        }
    }

    /**
     * Sharing a question by Email
     */
    fun shareQuestion(email: String, questionUrl: String): LiveData<BaseData<HealthStatus>> {
        val mData = MutableLiveData<BaseData<HealthStatus>>()

        return Transformations.map(repository.shareQuestion(email, questionUrl)) { data ->
            data.let {

                if (it.success) {
                    mData.value = BaseData(true, it.data)
                } else {
                    mData.value = BaseData(false, null)
                }

                hideLoading()

                mData.value
            }
        }
    }

    /**
     * Convert the Question result from service to QuestionModel for using on View
     *
     * @param data the tv show response
     * @return the list or one object the QuestionModel
     */
    private fun transformQuestionList(data: List<Question>): List<QuestionModel> {
        val result = arrayListOf<QuestionModel>()

        for (question in data) {
            result.add(question.toModel())
        }

        return result
    }

    /**
     * Increase the offset to get more elements
     */
    fun moreElements() {
        questionsOffset += questionsLimit
    }

    /**
     * Executing a new request again
     */
    fun newRequest() {
        questionsLimit = 10
        questionsOffset = 0
        questionModelList = ArrayList()
        filter = ""
    }

}