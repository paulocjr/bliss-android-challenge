package com.bliss.challenge.viewmodel

import androidx.databinding.ObservableField
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.Transformations
import com.bliss.challenge.models.BaseData
import com.bliss.challenge.repository.health.HealthRepository
import com.bliss.challenge.service.models.HealthStatus
import com.bliss.challenge.viewmodel.base.BaseViewModel

/**
 * Created by pcamilo on 16/10/2019
 */
class HealthViewModel(healthRepository: HealthRepository)  : BaseViewModel() {

    private var repository: HealthRepository = healthRepository

    /**
     * Check the status of backend server
     */
    fun getServiceStatus(): LiveData<BaseData<HealthStatus>> {

        return Transformations.map(repository.getServiceStatus()) { data ->
            data.let {

                val mData = MutableLiveData<BaseData<HealthStatus>>()

                if (it.success) {
                    mData.value = BaseData(true, it.data)
                } else {
                    mData.value = BaseData(false, null)
                }

                mData.value
            }
        }
    }
}