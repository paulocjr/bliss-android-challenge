package com.bliss.challenge.di.scope

import javax.inject.Scope

/**
 * Created by paulo on 15-10-2019.
 */
@Scope
@kotlin.annotation.Retention(AnnotationRetention.RUNTIME)
annotation class Activity