package com.bliss.challenge.di.app

import android.app.Application
import android.content.Context
import com.bliss.challenge.repository.health.HealthRepository
import com.bliss.challenge.repository.question.QuestionRepository
import com.bliss.challenge.service.APIClient
import dagger.Component
import javax.inject.Singleton

/**
 * Created by paulo on 15-10-2019.
 */
@Singleton
@Component(
    modules = [AppModule::class, ViewModelBuilder::class])
interface AppComponent {
    fun application() = Application()
    fun context(): Context
    fun apiClient(): APIClient
    fun questionRepository() : QuestionRepository
    fun healthRepository() : HealthRepository
}