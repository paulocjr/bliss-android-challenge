package com.bliss.challenge.di.app

import androidx.lifecycle.ViewModelProvider
import com.bliss.challenge.viewmodel.base.ViewModelFactory
import dagger.Binds
import dagger.Module

/**
 * Created by paulo on 15-10-2019.
 */
@Module
abstract class ViewModelBuilder {

    // ViewModel Factory
    @Binds
    abstract fun bindViewModelFactory(factory: ViewModelFactory): ViewModelProvider.Factory

}
