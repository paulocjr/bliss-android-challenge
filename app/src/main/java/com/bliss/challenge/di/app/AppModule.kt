package com.bliss.challenge.di.app

import android.app.Application
import android.content.Context
import com.bliss.challenge.service.APIClient
import dagger.Module
import dagger.Provides
import javax.inject.Singleton

/**
 * Created by paulo on 15-10-2019.
 */
@Module
class AppModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideApplication() : Application = app

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideApiClient() = APIClient()
}