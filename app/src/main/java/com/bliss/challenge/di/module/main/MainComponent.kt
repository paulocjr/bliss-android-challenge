package com.bliss.challenge.di.module.main

import com.bliss.challenge.di.app.AppComponent
import com.bliss.challenge.di.scope.Activity
import com.bliss.challenge.view.main.MainActivity
import dagger.Component

/**
 * Created by paulo on 15-10-2019.
 */
@Activity
@Component(dependencies = [AppComponent::class])
interface MainComponent {
    fun inject(activity: MainActivity)
}