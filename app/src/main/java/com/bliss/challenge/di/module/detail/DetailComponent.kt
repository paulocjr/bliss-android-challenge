package com.bliss.challenge.di.module.detail

import com.bliss.challenge.di.app.AppComponent
import com.bliss.challenge.di.scope.Activity
import com.bliss.challenge.view.detail.QuestionDetailActivity
import dagger.Component

/**
 * Created by paulo on 17-10-2019.
 */
@Activity
@Component(dependencies = [AppComponent::class])
interface DetailComponent {
    fun inject(activity: QuestionDetailActivity)
}