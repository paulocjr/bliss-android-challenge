package com.bliss.challenge.utils

import android.annotation.SuppressLint
import java.text.SimpleDateFormat

/**
 * Created by pcamilo on 16/10/2019
 */
class Utils {
    companion object {

        /**
         * Method to format the date
         */
        @SuppressLint("SimpleDateFormat")
        fun formatDate(value: String): String {
            try {
                val format = SimpleDateFormat("yyyy-MM-dd")
                val date = format.parse(value)
                val formatted = SimpleDateFormat("dd - MM - yyyy")
                return formatted.format(date)
            } catch (e: Exception) {
                e.printStackTrace()
            }
            return ""
        }
    }
}