package com.bliss.challenge

import android.app.Application
import com.bliss.challenge.di.app.AppComponent
import com.bliss.challenge.di.app.AppModule
import com.bliss.challenge.di.app.DaggerAppComponent

/**
 * Created by paulo on 15-10-2019.
 */
class BlissApplication : Application() {

    companion object {
        private lateinit var mBlissApplication: BlissApplication
        fun getInstance(): BlissApplication = mBlissApplication
    }

    private var mAppComponent: AppComponent? = null

    override fun onCreate() {
        super.onCreate()
        mBlissApplication = this
        initInjector()
    }

    /**
     * Initialize the dagger component
     */
    private fun initInjector() {
        mAppComponent = DaggerAppComponent.builder()
            .appModule(AppModule(this))
            .build()
    }

    fun getAppComponent() = mAppComponent
}