package com.bliss.challenge.models

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.bliss.challenge.BR
import com.bliss.challenge.service.models.Choice

/**
 * Created by paulo on 15-10-2019.
 */
data class QuestionModel(var _id: Int,
                         var _question: String,
                         var _imageUrl: String,
                         var _thumbUrl: String,
                         var _publishedAt: String,
                         var _choices: List<Choice>) : BaseObservable() {

    var id: Int
        @Bindable get() = _id
        set(value) {
            _id = value
            notifyPropertyChanged(BR.id)
        }

    var question: String
        @Bindable get() = _question
        set(value) {
            _question = value
            notifyPropertyChanged(BR.question)
        }

    var imageUrl: String
        @Bindable get() = _imageUrl
        set(value) {
            _imageUrl = value
            notifyPropertyChanged(BR.imageUrl)
        }

    var thumbUrl: String
        @Bindable get() = _thumbUrl
        set(value) {
            _thumbUrl = value
            notifyPropertyChanged(BR.thumbUrl)
        }

    var publishedAt: String
        @Bindable get() = _publishedAt
        set(value) {
            _publishedAt = value
            notifyPropertyChanged(BR.publishedAt)
        }

    var choices: List<Choice>
        @Bindable get() = _choices
        set(value) {
            _choices = value
            notifyPropertyChanged(BR.choices)
        }

    companion object {
        val EXTRA_ID = "question_id"
        val EXTRA_NAME = "question_name"
        val EXTRA_IMAGE = "question_image"
    }
}