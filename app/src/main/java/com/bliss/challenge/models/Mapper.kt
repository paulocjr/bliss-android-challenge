package com.bliss.challenge.models

import com.bliss.challenge.service.models.Question
import com.bliss.challenge.utils.Utils

/**
 * Created by paulo on 15-10-2019.
 */
fun Question.toModel() = QuestionModel(
        _id = this.id,
        _question = question,
        _publishedAt = Utils.formatDate(publishedAt),
        _choices = choices,
        _imageUrl = imageUrl,
        _thumbUrl = thumbUrl
)
