package com.bliss.challenge.models

/**
 * Created by paulo on 15-10-2019.
 */
class BaseData<T>(var success: Boolean, var data: T?)