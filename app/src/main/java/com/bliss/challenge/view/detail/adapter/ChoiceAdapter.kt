package com.bliss.challenge.view.detail.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import android.widget.ImageView
import androidx.recyclerview.widget.RecyclerView
import com.bliss.challenge.databinding.LayoutItemChoiceBinding
import com.bliss.challenge.service.models.Choice
import com.varunest.sparkbutton.SparkEventListener

/**
 * Created by pcamilo on 16/10/2019
 */
class ChoiceAdapter() : RecyclerView.Adapter<ChoiceAdapter.ChoiceViewHolder>() {

    private var mListChoices = arrayListOf<Choice>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChoiceViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutItemChoiceBinding.inflate(inflater)
        return ChoiceViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return mListChoices.size
    }

    override fun onBindViewHolder(holder: ChoiceViewHolder, position: Int) {
        holder.bind(mListChoices[position])
    }

    inner class ChoiceViewHolder(private val binding: LayoutItemChoiceBinding) : RecyclerView.ViewHolder(binding.root) {

        fun bind(item: Choice) {
            binding.tvQuestionName.text = item.choice
            binding.tvQuestionVotes.text = item.votes.toString()
            binding.executePendingBindings()
        }
    }

    /**
     * Set all data in layout item
     *
     * @param data the data with elements
     */
    fun submitList(data: List<Choice>) {
        this.mListChoices = data as ArrayList<Choice>
        notifyDataSetChanged()
    }
}