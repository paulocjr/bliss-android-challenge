package com.bliss.challenge.view.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.bliss.challenge.R
import com.bliss.challenge.databinding.LayoutItemQuestionBinding
import com.bliss.challenge.models.QuestionModel
import com.squareup.picasso.Picasso
import de.hdodenhof.circleimageview.CircleImageView

/**
 * Created by pcamilo on 16/10/2019
 */
class QuestionListAdapter(listener: QuestionListAdapterListener) : RecyclerView.Adapter<QuestionListAdapter.QuestionViewHolder>() {

    private var mListQuestions = arrayListOf<QuestionModel>()
    private var mListener: QuestionListAdapterListener = listener
    private var loading = false

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): QuestionViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = LayoutItemQuestionBinding.inflate(inflater)
        return QuestionViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return mListQuestions.size
    }

    override fun onBindViewHolder(holder: QuestionViewHolder, position: Int) {
        holder.bind(mListQuestions[position])

        if (position == itemCount - 1  && loading)
            holder.mLoadingBar.visibility = View.VISIBLE
        else
            holder.mLoadingBar.visibility = View.GONE
    }

    // view holder for layout = layout_item_question.xml
    inner class QuestionViewHolder(private val binding: LayoutItemQuestionBinding) : RecyclerView.ViewHolder(binding.root) {
        val mLoadingBar = binding.loading
        fun bind(item: QuestionModel) {
            binding.model = item
            setupImageView(binding.imgQuestion, item._thumbUrl)

            binding.rootView.setOnClickListener {
                mListener.onQuestionClicked(item, binding.tvQuestionName, binding.imgQuestion)
            }

            binding.executePendingBindings()
        }
    }
    /**
     * Set image from URL on ImageView in each layout item displayed
     */
    fun setupImageView(
        imageView: CircleImageView,
        url: String) {

        Picasso.get()
            .load(url)
            .error(R.drawable.ic_no_picture)
            .into(imageView)
    }

    /**
     * Set all data in layout item
     *
     * @param data the data with elements
     */
    fun submitList(data: List<QuestionModel>) {
        this.mListQuestions = data as ArrayList<QuestionModel>
        notifyDataSetChanged()
    }

    /**
     * Show loading to more items
     */
    fun showLoading(loading: Boolean) {
        this.loading = loading
        notifyDataSetChanged()
    }

    interface QuestionListAdapterListener {

        /**
         * On question item clicked
         */
        fun onQuestionClicked(question: QuestionModel, title: TextView, image: ImageView)
    }
}