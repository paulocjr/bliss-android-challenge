package com.bliss.challenge.view.detail

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.graphics.PorterDuff
import android.util.Log
import android.view.LayoutInflater
import android.view.MenuItem
import android.view.View
import android.widget.EditText
import android.widget.ImageView
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bliss.challenge.R
import com.bliss.challenge.databinding.ActivityQuestionDetailBinding
import com.bliss.challenge.di.module.detail.DaggerDetailComponent
import com.bliss.challenge.models.QuestionModel
import com.bliss.challenge.repository.question.QuestionRepository
import com.bliss.challenge.service.models.Choice
import com.bliss.challenge.view.base.BaseActivity
import com.bliss.challenge.view.detail.adapter.ChoiceAdapter
import com.bliss.challenge.view.splash.SplashActivity
import com.bliss.challenge.viewmodel.QuestionViewModel
import com.bliss.challenge.viewmodel.base.getViewModel
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.squareup.picasso.Picasso
import com.varunest.sparkbutton.SparkEventListener
import javax.inject.Inject


class QuestionDetailActivity : BaseActivity<ActivityQuestionDetailBinding>(R.layout.activity_question_detail) {

    @Inject
    lateinit var questionRepository: QuestionRepository
    private val mQuestionViewModel: QuestionViewModel by lazy {
        getViewModel { QuestionViewModel(questionRepository) }
    }

    lateinit var mQuestionModel: QuestionModel
    private var mChoiceAdapter: ChoiceAdapter? = null
    private var mSnackBar: Snackbar? = null

    override fun initInjectors() {
        DaggerDetailComponent.builder()
                .appComponent(getAppComponent())
                .build().inject(this)
    }

    override fun initBinding() {
        getBinding().apply {
            viewModel = mQuestionViewModel
            lifecycleOwner = this@QuestionDetailActivity
        }

        if (checkIntentDeepLink())
            questionDeepLinking()
         else
            receiveDataFromBundle()

        getBinding().ivShareQuestion.setOnClickListener {
            shareQuestion()
        }

        getBinding().sparkButton.setEventListener(object : SparkEventListener {
            override fun onEventAnimationEnd(button: ImageView?, buttonState: Boolean) {

            }

            override fun onEventAnimationStart(button: ImageView?, buttonState: Boolean) {
            }

            override fun onEvent(button: ImageView?, buttonState: Boolean) {
                if (buttonState) {
                    updateQuestion()
                }
            }
        })
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        // Respond to the action bar's Up/Home button
        if (item.itemId == android.R.id.home) {
            onBackPressed()
            return true
        }
        return super.onOptionsItemSelected(item)
    }

    /**
     * Share question action
     */
    private fun shareQuestion() {
        val questionId = mQuestionModel.id
        val questionShared = String.format(getString(R.string.url_share_question), questionId)
        Log.v(QuestionDetailActivity::javaClass.name, "sharing question : $questionShared")

        val layoutInflater = LayoutInflater.from(this)
        val viewDialog = layoutInflater.inflate(R.layout.layout_share_question, null)

        //Creating a alert dialog
        val alertDialog = AlertDialog.Builder(this)
        alertDialog.setView(viewDialog)

        val inputEmail = viewDialog.findViewById<EditText>(R.id.userInputDialog)

        alertDialog.setCancelable(false)
        alertDialog
                .setPositiveButton("Send") { _, _ ->
                    val emailDestination = inputEmail.text.toString()
                    if (emailDestination.isEmpty()) {
                        Toast.makeText(this, getString(R.string.message_email_empty), Toast.LENGTH_SHORT).show()
                    } else {
                        sendQuestionByEmail(emailDestination, questionShared)
                    }
            }
                .setNegativeButton("Cancel") { dialog, _ ->
                    dialog?.dismiss()
            }

        val alertDialogAndroid = alertDialog.create()
        alertDialogAndroid.show()
    }

    private fun sendQuestionByEmail(email: String, questionShared: String) {
        Log.v(QuestionDetailActivity::javaClass.name, "sharing question : email = $email question = $questionShared")

        mQuestionViewModel.shareQuestion(email, questionShared).observe(this, Observer { res ->
            if (res != null && res.success) {
                showSnackBar(getString(R.string.shared_success))
            } else {
                setErrorLayout()
            }
        })
    }

    /**
     * Message success sharing question by email
     */
    @SuppressLint("NewApi")
    private fun showSnackBar(message: String) {
        mSnackBar = Snackbar.make(findViewById(R.id.content_main), message, BaseTransientBottomBar.LENGTH_SHORT)
        mSnackBar?.setBackgroundTint(getColor(R.color.colorConnection))
        mSnackBar?.show()
    }

    private fun receiveDataFromBundle() {
        val bundle = intent.extras
        if (bundle == null || !bundle.containsKey(QuestionModel.EXTRA_ID)) {
            setErrorLayout()
        } else {
            mQuestionModel = QuestionModel(bundle.getInt(QuestionModel.EXTRA_ID), bundle.getString(QuestionModel.EXTRA_NAME), bundle.getString(QuestionModel.EXTRA_IMAGE), "", "", arrayListOf())
            getBinding().tbQuestionDetail.title = mQuestionModel._question
            setupToolbar()
            makeImageToolbarTransition(mQuestionModel)

            //Call the service to get all details
            getQuestionDetail(mQuestionModel._id)
        }
    }

    /**
     * Check if has bundle with question id used on DeepLinking
     */
    private fun checkIntentDeepLink(): Boolean {
        val bundle = intent.extras
        return bundle != null && bundle.containsKey(SplashActivity.PARAM_ID)
    }

    /**
     * Open question by identifier using DeepLinking functionality
     */
    private fun questionDeepLinking() {
        val bundle = intent.extras
        try {
            getQuestionDetail((bundle?.get(SplashActivity.PARAM_ID) as String).toInt())
        } catch (e: Exception) {
            setErrorLayout()
        }
    }

    /**
     * Get Question detail by identifier
     *
     * @param id the identifier of QuestionModel
     */
    private fun getQuestionDetail(id: Int) {
        mQuestionViewModel.getQuestionById(id).observe(this, Observer { res ->
            if (res != null && res.success) {
                setQuestionDetails(res.data!!)
            } else {
                setErrorLayout()
            }
        })
    }

    /**
     * Populate de viw with data retrieved of Service
     *
     * @param questionModel the QuestionModel with details
     */
    private fun setQuestionDetails(questionModel: QuestionModel) {
        getBinding().model = questionModel
        getBinding().tbQuestionDetail.title = questionModel.question
        setupToolbar()
        setupImageToolbar(questionModel.imageUrl)
        setupRecycleViewChoices(questionModel.choices)
    }

    /**
     * Updates the question using identifier
     */
    private fun updateQuestion() {
        mQuestionViewModel.updateQuestion(mQuestionModel.id).observe(this, Observer { res ->
            if (res != null && res.success) {
                setQuestionDetails(res.data!!)
                showSnackBar(getString(R.string.updated_success))
            } else {
                setErrorLayout()
            }
        })
    }

    /**
     * Set the list with all choices that question
     */
    private fun setupRecycleViewChoices(choices: List<Choice>) {
        choices.let {
            if (mChoiceAdapter == null) {
                mChoiceAdapter = ChoiceAdapter()
                mChoiceAdapter?.submitList(choices)

                getBinding().rvChoices.adapter = mChoiceAdapter

                if (getBinding().rvChoices.itemDecorationCount > 0)
                    getBinding().rvChoices.removeItemDecorationAt(0)

                val manager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
                getBinding().rvChoices.layoutManager = manager
                getBinding().rvChoices.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
            } else {
                mChoiceAdapter?.submitList(choices)
            }
         }
    }

    /**
     * Show the layout error from service
     */
    private fun setErrorLayout() {
        getBinding().appBarLayout.visibility = View.GONE
        getBinding().contentDetailError.visibility = View.VISIBLE
    }

    /**
     * Make animation when I receive the image of QuestionModel or Bundle param
     *
     * @param questionModel the QuestionModel parameter
     */
    private fun makeImageToolbarTransition(questionModel: QuestionModel) {
        setupImageToolbar(questionModel.imageUrl)
    }

    /**
     * Set the toolbar on activity
     *
     * @param imageUrl the image that will be displayed on AppBar
     */
    private fun setupImageToolbar(imageUrl: String) {
        Picasso.get()
                .load(imageUrl)
                .into(getBinding().ivQuestionImage)
    }

    /**
     * Setup toolbar to activity with data binding
     */
    private fun setupToolbar() {
        setSupportActionBar(getBinding().tbQuestionDetail)

        val upArrow = resources.getDrawable(R.drawable.ic_arrow_back_black_24dp).mutate()
        upArrow.setColorFilter(resources.getColor(R.color.colorWhite), PorterDuff.Mode.SRC_ATOP)
        supportActionBar?.setHomeAsUpIndicator(upArrow)
        supportActionBar?.setDisplayHomeAsUpEnabled(true)

        getBinding().collapsingToolbar.setExpandedTitleColor(resources.getColor(R.color.colorWhite))
        getBinding().collapsingToolbar.setCollapsedTitleTextColor(resources.getColor(R.color.colorWhite))
    }
}
