package com.bliss.challenge.view.splash

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.lifecycle.Observer
import com.bliss.challenge.R
import com.bliss.challenge.databinding.ActivitySplashBinding
import com.bliss.challenge.di.module.splash.DaggerSplashComponent
import com.bliss.challenge.repository.health.HealthRepository
import com.bliss.challenge.view.base.BaseActivity
import com.bliss.challenge.view.detail.QuestionDetailActivity
import com.bliss.challenge.view.main.MainActivity
import com.bliss.challenge.viewmodel.HealthViewModel
import com.bliss.challenge.viewmodel.base.getViewModel
import javax.inject.Inject

class SplashActivity : BaseActivity<ActivitySplashBinding>(R.layout.activity_splash) {

    companion object {
        const val PARAM_FILTER = "filter"
        const val PARAM_ID = "id"
        const val QUESTION_ID = "question_id"
        const val QUESTION_FILTER = "question_filter"
    }

    @Inject
    lateinit var healthRepository: HealthRepository
    private val mHealthViewModel: HealthViewModel by lazy {
        getViewModel { HealthViewModel(healthRepository) }
    }

    override fun initInjectors() {
        DaggerSplashComponent.builder()
            .appComponent(getAppComponent())
            .build().inject(this)
    }

    override fun initBinding() {
        getBinding().apply {
            viewModel = mHealthViewModel
            lifecycleOwner = this@SplashActivity
        }

        if (hasDeepLink()) {
            openSpecificActivity()
        } else {
            checkHealthService()
        }

        getBinding().tvRetry.setOnClickListener {
            retryService()
        }
    }

    private fun openSpecificActivity() {
        val intent = intent.data
        if (intent != null) {
            val questionUri = Uri.parse(this.intent.dataString)
            when {
                questionUri.getQueryParameter(PARAM_FILTER) != null -> startActivityBundle(QUESTION_FILTER, questionUri?.getQueryParameter(PARAM_FILTER))
                questionUri.getQueryParameter(PARAM_ID) != null -> startActivityBundle(QUESTION_ID, questionUri?.getQueryParameter(PARAM_ID))
                else -> checkHealthService()
            }
        } else {
            checkHealthService()
        }
    }

    private fun hasDeepLink(): Boolean {
        val intent = intent
        return if (intent != null) {
            val data = intent.data
            data != null
        } else {
            false
        }
    }

    /**
     * Call the service again
     */
    private fun retryService() {
        hideError()
        checkHealthService()
    }

    /**
     * Check the status of backend server
     */
    private fun checkHealthService() {
        mHealthViewModel.getServiceStatus().observe(this, Observer { res ->
            if (res.data?.status != null) {
                hideError()

                if (res.data?.status.equals(getString(R.string.service_result_ok)))
                    startMainActivity()
                else
                    showError()
            } else {
                showError()
            }
        })
    }

    private fun startMainActivity() {
        startActivity(Intent(this, MainActivity::class.java))
        overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out)
        finish()
    }

    private fun showError() {
        getBinding().contentError.visibility = View.VISIBLE
    }

    private fun hideError() {
        getBinding().contentError.visibility = View.GONE
    }

    private fun startActivityBundle(type: String, value: String?) {
        var intentAction: Intent? = null

        when(type) {
            QUESTION_FILTER -> {
                intentAction = Intent(this, MainActivity::class.java)
                intentAction.putExtra(PARAM_FILTER, value)
            }
            QUESTION_ID -> {
                intentAction = Intent(this, QuestionDetailActivity::class.java)
                intentAction.putExtra(PARAM_ID, value)
            } else -> {
                checkHealthService()
            }
        }

        startActivity(intentAction)
        overridePendingTransition(R.anim.anim_fade_in, R.anim.anim_fade_out)
        finish()
    }
}
