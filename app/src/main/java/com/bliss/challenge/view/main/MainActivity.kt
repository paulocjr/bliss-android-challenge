package com.bliss.challenge.view.main

import android.annotation.SuppressLint
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.ConnectivityManager
import android.transition.Explode
import android.transition.Fade
import android.transition.TransitionSet
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.widget.SearchView
import androidx.core.app.ActivityOptionsCompat
import androidx.core.util.Pair
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.bliss.challenge.R
import com.bliss.challenge.databinding.ActivityMainBinding
import com.bliss.challenge.di.module.main.DaggerMainComponent
import com.bliss.challenge.models.QuestionModel
import com.bliss.challenge.repository.question.QuestionRepository
import com.bliss.challenge.view.base.BaseActivity
import com.bliss.challenge.view.detail.QuestionDetailActivity
import com.bliss.challenge.view.main.adapter.QuestionListAdapter
import com.bliss.challenge.view.receiver.ConnectivityReceiver
import com.bliss.challenge.view.splash.SplashActivity
import com.bliss.challenge.viewmodel.QuestionViewModel
import com.bliss.challenge.viewmodel.base.getViewModel
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import javax.inject.Inject



@SuppressLint("Registered")
class MainActivity : BaseActivity<ActivityMainBinding>(R.layout.activity_main),
    QuestionListAdapter.QuestionListAdapterListener, ConnectivityReceiver.ConnectivityReceiverListener {

    private var mQuestionAdapter: QuestionListAdapter? = null
    private var isFetchingQuestions: Boolean = false
    private var mSnackBar: Snackbar? = null

    @Inject
    lateinit var questionRepository: QuestionRepository
    private val mQuestionViewModel: QuestionViewModel by lazy {
        getViewModel { QuestionViewModel(questionRepository) }
    }

    override fun initInjectors() {
        DaggerMainComponent.builder()
            .appComponent(getAppComponent())
            .build().inject(this)
    }

    override fun initBinding() {
        initBroadcastReceiver()

        getBinding().apply {
            viewModel = mQuestionViewModel
            lifecycleOwner = this@MainActivity
        }

        if (checkIntentDeepLink())
            getQuestionFilter()
         else
            getQuestions(isLoading = true)

        getBinding().searchView.setOnQueryTextListener(object : SearchView.OnQueryTextListener{

            override fun onQueryTextSubmit(query: String?): Boolean {
                mQuestionViewModel.newRequest()
                mQuestionViewModel.filter = query.toString()
                mQuestionAdapter = null
                getQuestions(isLoading = true)
                return false
            }

            override fun onQueryTextChange(newText: String?): Boolean {
                return false
            }
        })

        getBinding().searchView.setOnCloseListener {
            mQuestionViewModel.newRequest()
            mQuestionAdapter = null
            getQuestions(isLoading = true)
            false
        }
    }

    override fun onResume() {
        super.onResume()
        ConnectivityReceiver.connectivityReceiverListener = this
    }

    override fun onQuestionClicked(question: QuestionModel, title: TextView, image: ImageView) {
        openQuestionDetail(question, title, image)
    }

    /**
     * Open a question detail using SharedElements
     */
    private fun openQuestionDetail(
        question: QuestionModel,
        title: TextView,
        image: ImageView
    ) {
        val intentQuestionDetails = Intent(this, QuestionDetailActivity::class.java)
        intentQuestionDetails.putExtra(QuestionModel.EXTRA_ID, question._id)
        intentQuestionDetails.putExtra(QuestionModel.EXTRA_NAME, question._question)
        intentQuestionDetails.putExtra(QuestionModel.EXTRA_IMAGE, question._imageUrl)

        val transitionSet = TransitionSet()
        transitionSet.addTransition(Fade())

        // set an exit and enter transition
        window.enterTransition = Explode()
        window.exitTransition = Explode()

        val pairImage = Pair.create<View, String>(image, getString(R.string.question_image))
        val pairTitle = Pair.create<View, String>(title, getString(R.string.question_name))
        val opt = ActivityOptionsCompat.makeSceneTransitionAnimation(this, pairImage, pairTitle)

        startActivity(intentQuestionDetails, opt.toBundle())
    }

    @SuppressLint("NewApi")
    override fun onNetworkConnectionChanged(isConnected: Boolean) {
        if (!isConnected) {
            snackConnection(getString(R.string.message_no_connection), BaseTransientBottomBar.LENGTH_INDEFINITE)
            getBinding().contentError.visibility = View.VISIBLE
            getBinding().rvQuestions.visibility = View.GONE
            getBinding().searchView.visibility = View.GONE
        } else {
            getBinding().contentError.visibility = View.GONE
            getBinding().rvQuestions.visibility = View.VISIBLE
            getBinding().searchView.visibility = View.VISIBLE
            mSnackBar?.dismiss()
        }
    }

    /**
     * Search question using filter from deep linking
     */
    private fun getQuestionFilter() {
        val bundle = intent.extras
        try {
            mQuestionViewModel.filter = bundle?.getString(SplashActivity.PARAM_FILTER).toString()
            getBinding().searchView.setQuery(mQuestionViewModel.filter, true)
            getQuestions(isLoading = true)
        } catch (e: Exception) {
            mQuestionViewModel.newRequest()
            getQuestions(isLoading = true)
        }
    }

    /**
     * Show message no internet connection
     */
    private fun snackConnection(message: String, duration: Int) {
        mSnackBar = Snackbar.make(findViewById(R.id.content_main), message, duration)
        mSnackBar?.show()
    }

    /**
     * Get all questions from service
     */
    private fun getQuestions(isLoading: Boolean) {

        if (isLoading) {
            getBinding().rvQuestions.visibility = View.GONE
            mQuestionViewModel.showLoading()
        } else {
            mQuestionViewModel.hideLoading()
        }

        mQuestionViewModel.getQuestions().observe(this, Observer { res ->
            if (res != null && res.success) {
                updateQuestions(res.data)
            }
        })

        isFetchingQuestions = false

        mQuestionAdapter?.let {
            it.showLoading(false)
        }
    }

    /**
     * Initialize the BroadcastReceiver to lister internet connection
     */
    private fun initBroadcastReceiver() {
        registerReceiver(ConnectivityReceiver(), IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION))
    }

    /**
     * Update the RecycleView with all data retrieved from service
     *
     * @param questions the list of questions
     */
    private fun updateQuestions(questions: List<QuestionModel>?) {
        getBinding().rvQuestions.visibility = View.VISIBLE

        if (mQuestionAdapter == null) {
            mQuestionAdapter = QuestionListAdapter(this)
            mQuestionAdapter?.submitList(questions!!)
            getBinding().rvQuestions.adapter = mQuestionAdapter

            if (getBinding().rvQuestions.itemDecorationCount > 0)
                getBinding().rvQuestions.removeItemDecorationAt(0)

            setupOnScrollListener()
            getBinding().rvQuestions.addItemDecoration(DividerItemDecoration(this, LinearLayoutManager.VERTICAL))
        } else {
            mQuestionAdapter?.submitList(questions!!)
        }
    }

    /**
     * Set up the scroll listener of RecycleView
     */
    private fun setupOnScrollListener() {
        val manager = LinearLayoutManager(this, RecyclerView.VERTICAL, false)
        getBinding().rvQuestions.layoutManager = manager

        getBinding().rvQuestions.addOnScrollListener(object : RecyclerView.OnScrollListener() {
            override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
                if (recyclerView.adapter != null && manager.findLastCompletelyVisibleItemPosition() == recyclerView.adapter!!.itemCount - 1) {
                    if (!isFetchingQuestions) {
                        mQuestionAdapter?.showLoading(true)
                        isFetchingQuestions = true
                        mQuestionViewModel.moreElements()
                        getQuestions(false)
                    }
                }
            }
        })
    }

    /**
     * Check if has bundle with question filter used on DeepLinking
     */
    private fun checkIntentDeepLink(): Boolean {
        val bundle = intent.extras
        return bundle != null && bundle.containsKey(SplashActivity.PARAM_FILTER)
    }

}
