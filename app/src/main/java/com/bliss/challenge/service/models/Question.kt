package com.bliss.challenge.service.models

import com.google.gson.annotations.SerializedName

/**
 * Created by paulo on 15-10-2019.
 */
class Question(@SerializedName("id") val id: Int,
               @SerializedName("question") val question: String,
               @SerializedName("image_url") val imageUrl: String,
               @SerializedName("thumb_url") val thumbUrl: String,
               @SerializedName("published_at") val publishedAt: String,
               @SerializedName("choices") val choices: List<Choice>)