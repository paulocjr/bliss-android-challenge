package com.bliss.challenge.service.models

import com.google.gson.annotations.SerializedName

/**
 * Created by pcamilo on 16/10/2019
 */
class Choice(@SerializedName("choice") val choice: String, @SerializedName("votes") val votes: Int)