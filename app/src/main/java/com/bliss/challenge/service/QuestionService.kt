package com.bliss.challenge.service

import com.bliss.challenge.service.models.HealthStatus
import com.bliss.challenge.service.models.Question
import retrofit2.Call
import retrofit2.http.*

/**
 * Created by paulo on 15-10-2019.
 */
interface QuestionService {

    /**
     * Retrieve all questions using filters
     */
    @GET("questions")
    fun getQuestions(@Query("limit") limit: Int,
                     @Query("offset") offset: Int,
                     @Query("filter") filter: String): Call<List<Question>>

    /**
     * Retrieve the question by identifier
     */
    @GET("questions/{id}")
    fun getQuestionById(@Path("id") id: Int): Call<Question>

    /**
     * Update the question by identifier
     */
    @PUT("questions/{id}")
    fun updateQuestion(@Path("id") id: Int): Call<Question>

    /**
     * Share a question by Email
     */
    @POST("share")
    fun shareQuestion(@Query("destination_email") email: String,
                        @Query("content_url") contentUrl: String): Call<HealthStatus>
}