package com.bliss.challenge.service.models

import com.google.gson.annotations.SerializedName

/**
 * Created by paulo on 15-10-2019.
 */
class HealthStatus (@SerializedName("status") val status: String)