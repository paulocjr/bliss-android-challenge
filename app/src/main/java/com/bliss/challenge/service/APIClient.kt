package com.bliss.challenge.service

import com.bliss.challenge.BuildConfig
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import java.util.concurrent.TimeUnit

/**
 * Created by paulo on 15-10-2019.
 */
class APIClient() {

    private var mRetrofit: Retrofit

    init {
        mRetrofit = Retrofit
            .Builder()
            .baseUrl(BuildConfig.APP_BASE_URL)
            .client(getOkHttpClient())
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }

    private fun getLoggingCapableHttpClient(): HttpLoggingInterceptor {
        val mLogging = HttpLoggingInterceptor()
        mLogging.level = HttpLoggingInterceptor.Level.BODY

        return mLogging
    }

    private fun getOkHttpClient() : OkHttpClient {
        return OkHttpClient()
            .newBuilder()
            .connectTimeout(30, TimeUnit.SECONDS)
            .readTimeout(30, TimeUnit.SECONDS)
            .addInterceptor(getLoggingCapableHttpClient())
            .build()
    }

    fun getRetrofit() = mRetrofit
}