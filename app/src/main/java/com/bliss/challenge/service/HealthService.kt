package com.bliss.challenge.service

import com.bliss.challenge.service.models.HealthStatus
import retrofit2.Call
import retrofit2.http.GET

/**
 * Created by paulo on 15-10-2019.
 */
interface HealthService {

    /**
     * Retrieve the status server to make other requests
     */
    @GET("health")
    fun getServiceStatus(): Call<HealthStatus>
}