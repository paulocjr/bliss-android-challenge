package com.bliss.challenge.service.models

/**
 * Created by paulo on 15-10-2019.
 */
class BaseResponse<T>(val success: Boolean, val data: T?)