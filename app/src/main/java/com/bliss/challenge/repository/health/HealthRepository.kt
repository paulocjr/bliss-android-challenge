package com.bliss.challenge.repository.health

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bliss.challenge.service.APIClient
import com.bliss.challenge.service.HealthService
import com.bliss.challenge.service.models.BaseResponse
import com.bliss.challenge.service.models.HealthStatus
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by pcamilo on 16/10/2019
 */
class HealthRepository @Inject constructor (apiClient: APIClient) : IHealthRepository {

    private val TAG = HealthRepository::class.java.simpleName
    private val mHealthService: HealthService = apiClient.getRetrofit().create(HealthService::class.java)

    override fun getServiceStatus(): LiveData<BaseResponse<HealthStatus>> {
        val data: MutableLiveData<BaseResponse<HealthStatus>> = MutableLiveData()

        mHealthService.getServiceStatus()
            .enqueue(object : Callback<HealthStatus> {

                override fun onResponse(call: Call<HealthStatus>, response: Response<HealthStatus>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            data.value = BaseResponse(true, response.body())
                        } else {
                            data.value = BaseResponse(false, null)
                        }
                    } else {
                        data.value = BaseResponse(false, null)
                        Log.e(TAG, response.message())
                    }
                }

                override fun onFailure(call: Call<HealthStatus>, t: Throwable) {
                    data.value = BaseResponse(false, null)
                    t.message.let {
                        Log.e(TAG, it.toString())
                    }
                }
            })

        return data
    }
}