package com.bliss.challenge.repository.health

import androidx.lifecycle.LiveData
import com.bliss.challenge.service.models.BaseResponse
import com.bliss.challenge.service.models.HealthStatus

/**
 * Created by paulo on 15-10-2019.
 */
interface IHealthRepository {

    /**
     * Retrieve the status server to make other requests
     */
    fun getServiceStatus() : LiveData<BaseResponse<HealthStatus>>

}