package com.bliss.challenge.repository.question

import androidx.lifecycle.LiveData
import com.bliss.challenge.service.models.BaseResponse
import com.bliss.challenge.service.models.HealthStatus
import com.bliss.challenge.service.models.Question

/**
 * Created by paulo on 15-10-2019.
 */
interface IQuestionRepository {

    /**
     * Retrieve all questions using filters
     */
    fun getQuestions(limit: Int,
                     offset: Int,
                     filter: String) : LiveData<BaseResponse<List<Question>>>

    /**
     * Retrieve the question by identifier
     */
    fun getQuestionById(id: Int): LiveData<BaseResponse<Question>>

    /**
     * Share a question by email
     */
    fun shareQuestion(email: String, questionUrl: String): LiveData<BaseResponse<HealthStatus>>

    /**
     * Update the question by identifier
     */
    fun updateQuestion(id: Int): LiveData<BaseResponse<Question>>
}