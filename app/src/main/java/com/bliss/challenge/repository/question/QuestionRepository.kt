package com.bliss.challenge.repository.question

import android.util.Log
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.bliss.challenge.service.APIClient
import com.bliss.challenge.service.QuestionService
import com.bliss.challenge.service.models.BaseResponse
import com.bliss.challenge.service.models.HealthStatus
import com.bliss.challenge.service.models.Question
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import javax.inject.Inject

/**
 * Created by paulo on 15-10-2019.
 */
class QuestionRepository @Inject constructor (apiClient: APIClient) : IQuestionRepository {

    private val TAG = QuestionRepository::class.java.simpleName
    private val mQuestionService: QuestionService = apiClient.getRetrofit().create(QuestionService::class.java)

    override fun getQuestions(limit: Int, offset: Int, filter: String): LiveData<BaseResponse<List<Question>>> {
        val data: MutableLiveData<BaseResponse<List<Question>>> = MutableLiveData()

        mQuestionService.getQuestions(limit, offset, filter)
                .enqueue(object : Callback<List<Question>> {

                    override fun onResponse(call: Call<List<Question>>, response: Response<List<Question>>) {
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                data.value = BaseResponse(true, response.body())
                            } else {
                                data.value = BaseResponse(false, null)
                            }
                        } else {
                            data.value = BaseResponse(false, null)
                            Log.e(TAG, response.message())
                        }
                    }

                    override fun onFailure(call: Call<List<Question>>, t: Throwable) {
                        data.value = BaseResponse(false, null)
                        t.message.let {
                            Log.e(TAG, it.toString())
                        }
                    }
                })

        return data
    }

    override fun getQuestionById(id: Int): LiveData<BaseResponse<Question>> {
        val data: MutableLiveData<BaseResponse<Question>> = MutableLiveData()

        mQuestionService.getQuestionById(id)
            .enqueue(object : Callback<Question> {

                override fun onResponse(call: Call<Question>, response: Response<Question>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            data.value = BaseResponse(true, response.body())
                        } else {
                            data.value = BaseResponse(false, null)
                        }
                    } else {
                        data.value = BaseResponse(false, null)
                        Log.e(TAG, response.message())
                    }
                }

                override fun onFailure(call: Call<Question>, t: Throwable) {
                    data.value = BaseResponse(false, null)
                    t.message.let {
                        Log.e(TAG, it.toString())
                    }
                }
            })

        return data
    }

    override fun updateQuestion(id: Int): LiveData<BaseResponse<Question>> {
        val data: MutableLiveData<BaseResponse<Question>> = MutableLiveData()

        mQuestionService.updateQuestion(id)
            .enqueue(object : Callback<Question> {

                override fun onResponse(call: Call<Question>, response: Response<Question>) {
                    if (response.isSuccessful) {
                        if (response.body() != null) {
                            data.value = BaseResponse(true, response.body())
                        } else {
                            data.value = BaseResponse(false, null)
                        }
                    } else {
                        data.value = BaseResponse(false, null)
                        Log.e(TAG, response.message())
                    }
                }

                override fun onFailure(call: Call<Question>, t: Throwable) {
                    data.value = BaseResponse(false, null)
                    t.message.let {
                        Log.e(TAG, it.toString())
                    }
                }
            })

        return data
    }

    override fun shareQuestion(email: String, questionUrl: String): LiveData<BaseResponse<HealthStatus>> {
        val data: MutableLiveData<BaseResponse<HealthStatus>> = MutableLiveData()

        mQuestionService.shareQuestion(email, questionUrl)
                .enqueue(object : Callback<HealthStatus> {

                    override fun onResponse(call: Call<HealthStatus>, response: Response<HealthStatus>) {
                        if (response.isSuccessful) {
                            if (response.body() != null) {
                                data.value = BaseResponse(true, response.body())
                            } else {
                                data.value = BaseResponse(false, null)
                            }
                        } else {
                            data.value = BaseResponse(false, null)
                            Log.e(TAG, response.message())
                        }
                    }

                    override fun onFailure(call: Call<HealthStatus>, t: Throwable) {
                        data.value = BaseResponse(false, null)
                        t.message.let {
                            Log.e(TAG, it.toString())
                        }
                    }
                })

        return data
    }
}